﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Sleep
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly TimeSpan updateIntervals = TimeSpan.FromSeconds(1);
        DispatcherTimer dispatcherTimer;
        TimeSpan remaining = TimeSpan.FromSeconds(0);

        public MainWindow()
        {
            InitializeComponent();
            dispatcherTimer = new DispatcherTimer
            {
                Interval = updateIntervals
            };
            dispatcherTimer.Tick += Update;
        }

        private void Update(object sender, EventArgs e)
        {
            remaining -= updateIntervals;
            if (remaining.TotalSeconds <= 0)
            {
                Process.Start("shutdown", "/s /t 0");
            }
            Update();
        }

        private void Update()
        {
            Time.Text = remaining.ToString();
        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                remaining += TimeSpan.FromMinutes(2);
            }
            else
            {
                remaining -= TimeSpan.FromMinutes(Math.Min(2, remaining.TotalMinutes));
            }
            if (remaining.TotalSeconds > 0 && !dispatcherTimer.IsEnabled)
            {
                dispatcherTimer.Start();
            }
            if (remaining.TotalSeconds == 0)
            {
                dispatcherTimer.Stop();
            }
            Update();
        }
    }
}
